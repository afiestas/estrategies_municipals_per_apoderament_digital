# Propostes sobre Programari Lliure

### Experiències inspiradores

  * Projecte Decidim.org: s'utilitza ja a molts municipis
  * TODO: UK libreoffice, Austràlia, Nova Zelanda.
  * Migració a programari lliure de l'Ajuntament de Saragossa
  * Munic: https://www.kdab.com/the-limux-desktop-and-the-city-of-munich/ (beneficis guanyats amb coses que ha fet aquesta consultora). TODO: mirar com presentar-ho, és un cas controvertit.
  * Hi ha multitud d'organitzacions fent servir Linux i programari lliure (llista no actualitzada): https://en.wikipedia.org/wiki/List_of_Linux_adopters

### Referències

  * Campanya Public Money, Public Code: https://publiccode.eu/ca/
  * Projecte GNU: https://www.gnu.org/philosophy/government-free-software.en.html
  * Barcelona: https://ajuntament.barcelona.cat/digital/ca/documentacio

### dels objectius i la proposta programàtica

TODO.
